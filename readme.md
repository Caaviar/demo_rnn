
# Les réseaux de neurones


L'intelligence artificielle est omniprésente dans notre vie. Ses applications sont diverses : compréhension du langage naturel, traduction, reconnaissance visuelle, système autonome, machine learning, appareil photo, ...
Nous sommes encore bien loin de Terminator et de son automne semblable à l'homme. Aujourd'hui, les machines sont capables de reproduire des actions humaines (IA faible), cependant, elles ne développent pas de conscience (IA forte).
Le machine learning (ML) est une branche de l'IA, mais l'IA n'est pas que composé de machine learning. De plus, le Deep Learning est du machine learning particulier.
L'IA prend de l'ampleur et de l'intérêt grâce a l'émergence du Cloud Computing et du Big Data. Grace à ces technologies, la puissance de calcul est moins chère et plus grande, les données sont massives et plus facilement accessibles. Les machines peuvent dorénavant apprendre.
Le machine learning ou apprentissage automatique est capable de reproduire un comportement grâce a des algorithmes statistiques entraînés grâce à un grand volume de données. Le Deep Learning ou apprentissage profond va chercher à comprendre le fonctionnement avec plus de précision en analysant les données de manière plus profonde. On n'utilise plus une compréhension linéaire, mais une construction basée sur le fonctionnement du cerveau. On part du modèle le plus simple avec une entrée et une sortie. On peut ensuite rajouter autant de couches que voulu. Ce sont les réseaux de neurones.
L'intelligence artificielle est souvent réduite au machine learning et au Deep Learning. En réalité, il désigne la faculté de faire apprendre au ordinateur une tache et à la répéter.

## Histoire
On entend beaucoup parler des réseaux neuronaux ces dernières années, mais c'est un concept qui date du milieu du XXeme siècle. La méthode a été initiée par Frank Rosenblatt, psychologue américain, en 1957 avec la création du perception (algorithme d'apprentissage supervisé de classifieurs binaires). L'idée était de créer une méthode d'apprentissage inspirée et basée sur le fonctionnement du cerveau humain. Cependant, il n'a jamais été possible de tester par manque de capacité technologique. L'évolution des technologies a permis aux réseaux neuronaux de devenir une méthode incontournable du machine learning et de l'intelligence artificielle.   

## Exemple
Avant de commencer, nous pouvons utiliser différents exemples qui vont vous montrer que vous êtes déjà familier avec les réseaux de neurones.

Regardez cette image :

<img src="https://zupimages.net/up/19/20/aluh.png" alt="drawing" width="300"/>

Est-ce une table ? Des chaises ? Une table et des chaises ? Dans un premier temps, vous avez vu un objet grâce à vos yeux. L'information est ensuite traduite dans votre cerveau à un certain nombre de neurones qui a utilisé ce que vous avez vu dans votre vie pour définir l'objet en question. Logiquement, vous allez déterminer qu'il y a une table et deux chaises. Vous vous faites cette réflexion naturellement.

Vous souhaitez vendre votre maison ?

![ajoutlivre](https://zupimages.net/up/19/18/f9l6.jpg)

Afin de définir le prix de vente de votre bien, vous prenez en compte l'age de la maison, son état général, le nombre de chambres, de salle de bain. D'un autre coté, vous allez aussi vous renseigner sur l'état du marché immobilier dans votre zone pour pouvoir comparer votre maison aux autres. En connaissant l'ensemble de ces informations et en les confrontant avec les informations du marché immobilier dans votre zone, vous allez pouvoir estimer le prix de votre maison

Les réseaux neurones ont le même fonctionnement que les neurones dans votre cerveau. Ils vont prendre des informations en entrée (son, odeur, ...) afin de les confronter avec ce que vous connaissez déjà.


## Fonctionnement d'un reseau de neurones

L'attrait pour les réseaux de neurones est leur façon de reproduire la méthode de reconnaissance du cerveau.
Les réseaux neurones peuvent être utilisé dans toutes les situation comme pour prédire des décisions d'investissements, reconnaître l'écriture manuscrite ou analyser des images.

### Presentation

Pour comprendre comment fonctionne un réseau de neurones, il faut s'imaginer une boite noire qui prend des informations en entrée et qui va proposer une solution en sortie.

![](https://zupimages.net/up/19/18/f3la.png)

Par exemple, imaginez une voiture autonome. Elle va prendre une multitude de photographies en entrée afin de déterminer si elle doit tourner pour suivre une courbe, dépasser la voiture de devant, ou rester dans sa voie tout en faisant attention au reste de son environnement.

Le réseau neuronale possède plusieurs neurones regroupés en couches. Ces couches sont des colonnes de neurones reliés entre eux. Chaque neurone est relié au neurone d'une autre couche par des liens pondérés. Les connexions pondérées s'ajustent à l'aide d'un nombre qui lui est attaché.
Un neurone prend la valeur d'un neurone connecté et la multiplie par le poids de ses connexions. La somme de tous les neurones connectés est la valeur du biais des neurones.
Les neurones sont activés grâce à une fonction d'activation qui transforme mathématiquement la valeur et l'affecte au neurone connecté de la couche suivante et ainsi de suite sur l'ensemble des couches.
Le terme de "fonction d'activation" vient de "potentiel d'activation" en biologie. C'est un seuil de stimulation qui, dépassé, entraîne une réaction du neurone.
Il existe beaucoup de types de fonctions d'activation dont en voici quelques exemples :

 - Identité où f(x)=x
 - Marche où f(x) = 0 si x<0, 1 si x>0
 - Logistique où f(x) = 1/(1+e^-x)
 - ....


![rnn](https://zupimages.net/up/19/18/cwns.png)

Simplement, le réseau est un filtre de l'ensemble des réponses possible du problème afin que l'ordinateur prenne la meilleure décision possible.
Le poids de chaque lien est essentiel au bon fonctionnement et résultat du modèle, car c'est lui qui va permettre de calculer les valeurs. Trouver les bons poids est possible grâce à la phase d'apprentissage.

Le modèle peut se tromper dans l'exactitude de sa décision, mais il sera toujours "correct ", car il se base sur des caractéristiques spécifiques. Si un objet a des caractéristiques similaire à un autre, le réseau peut hésiter et sortie une réponse erronée.
Pour éviter cette erreur, les réseaux peuvent être équipés d'un mécanisme de retropropagation. Ceci permet au réseau de revenir en arrière pour vérifier l'ensemble des biais et de les ajuster si besoin. Le réseau va partir de la dernière à la premier couche pour reconfronter ses calculs.
On peut aussi transformer le réseau en un réseau neuronale récurent, impliquant des signaux qui vont de la premier a la dernière couche et inversement afin de corriger les poids en permanence. Ils sont particulièrement utiles pour prédire un senario suivant probable.

### La phase d'apprentissage

Pour entraîner un réseau de neurones, il faut un jeu de données supervisé. Chaque exemple (ligne) contient un ensemble de caractéristiques et la réponse souhaité.
Par exemple avec le jeu de données iris. C'est un jeu de données supervisé très connu. Il classe 3 types de pétale en fonction de caractéristique :

|Sepal_length|Sepal_width|Petal_lenght|Petal_width|Species|
|--|--|--|--|--|
|*Caracteristique*|*Caracteristique*|*Caracteristique*|*Caracteristique*|*Label*|
| 5 | 3.3 | 1.4 | 0.2 | Iris-setosa |
| 6.4 | 3.2 | 4.5 | 1.5 | Iris-Versicolor |
| 6.7 | 2.5 | 5 | 2 | Iris-Virginica |

En fonction des 4 variables, nous donnons au réseau des résultats attendu. Quand il activera ses différentes couches de neurones, il comparera les nouvelles caractéristiques avec celles qu'il a apprises afin de classer le nouvel individu dans une espèce.
L'apprentissage supervisé s'oppose à l'apprentissage non supervisé où on va demander au modèle de faire lui-même les groupes.
Il existe des réseaux de neurones basé sur des jeux de données non supervisé.

### Les types de reseaux neuronaux

Il existe plusieurs type de réseaux de neurones :

- Reseau neuronal FeedForward : c'est la version la plus basique des réseaux. Les données ne se déplacent que dans un sens en passant de couche en couche. Il n'y a pas d'algorithme de retropropagation donc aucun moyen de vérifier ni de corriger un résultat automatiquement a l'intérieur du reseau.
- Réseau neuronale de base radiale : les fonctions de base radiale considèrent la distance d'un point par rapport au centre. Ces réseaux ont deux couches : une couche ou les caractéristiques sont combinées avec la fonction radiale et la couche de sortie.
- Réseau neuronale automatique de Kohonen (chercheur finlandais) : il a théorisé le carte automatique adaptative. Elles servent à cartographier un espace réel pour étudier la répartition des données. Dans l'application des réseaux de neurones, la carte va prendre en entrée des vecteurs de dimensions arbitraire. L'ensemble des points de la carte vont se relier entre eux en fonction des poids calculés.
- Réseau neuronale récurrent : comme nous l'avons vu précedement, le but est de sauvegarder la sortie d'une couche afin de la renvoyer dans l'autre sens pour confronter le résultat obtenu dans la couche. On utilise la retropropagation pour faire la vérification avec d'assurer que la sortie est jusqu'à 99,9 % correcte.
- Réseau neuronale convolutionnel : ils sont similaires aux réseaux de neurones FeedForward, où les neurones ont des poids et des biais, mais sont plus adapté pour étudier le traitement du signal et les analyse d'image.
- Réseau neuronale modulaire : ensemble de réseau qui travaillent de manière indépendante tout en contribuant à la production. Chaque réseau reçoit en entrée des données uniques. C'est un gain de puissance et de rapidité dans l'exécution du modèle.  

 ### Les LSTM
 En lisant cette phrase, vous comprenez chaque mot de manière indépendante, mais pour en comprendre le sens global, vous associez de manière inconsciente ce mot avec celui d'avant et ainsi de suite. Les réseaux de neurones traditionnels sont incapables de reproduire ce mécanisme.
 Imaginez que vous souhaitiez classer le type d'évènement qui se produit à chaque moment de votre série préférée. Il n'est pas clair comment le réseau traditionnel pourrait utiliser son raisonnement sur les événements précédents pour prédire les suivant.
 Les réseaux de neurones récurrents peuvent résoudre ce problème car ils comportent des boucles qui permettent à l'information de persister.

![looprnn](https://zupimages.net/up/19/20/apxt.png)

Dans le schéma, ci-dessus, on a un morceau de réseau neuronale, hx examine certaines données d'entrée xt et délivre une valeur at. Une boucle permet de faire passer l'information d'une étape du réseau à l'autre.
Ces boucles rendent les réseaux neuronaux récurrents mystérieux. Mais on peut simplement dire qu'un réseau neuronale récurrent est plusieurs copies du même réseau, chacune transmettant un message à un successeur.

Les LSTM, sont un type de réseau neuronale récurrent particulier qui fonctionne, pour de nombreuses tâches, beaucoup mieux que la version standard. Ils peuvent à la différence des RNN garder en mémoire beaucoup plus longtemps les informations.

- Si nous essayons de prédire le dernier mot dans "les voitures roulent sur la *route*", nous n'avons pas besoin de contexte supplémentaire. Il est assez évident que le prochain mot sera route.
- Pensez à prédire le dernier mot du texte : "J'ai grandi en France... je parle couramment le *français*." Des informations récentes suggèrent que le mot suivant est probablement le nom d'une langue, mais si l'on veut réduire le nombre de langues, on a besoin du contexte de la France, plus en arrière.

Au fur et à mesure que l'information pertinente s'éloigne, le RNN aura de plus en plus de mal à la retrouver quand il en aura besoin.

Les réseaux neuronaux "Long Short Term Memory" (LSTM) sont un type particulier de RNN capable d'apprendre de retenir des dépendances à long terme.
Ils ont été introduits par Hochreiter & Schmidhuber à la fin des années 90.
Les LSTM ont une structure en chaîne, mais le module répétitif a une structure différente. Au lieu d'avoir une seule couche de réseau neuronale, il y en a quatre qui interagissent d'une manière très spéciale.
![](https://zupimages.net/up/19/20/eesq.jpg)
Nous suivrons le resonnement présent sur ce site afin de détailler le processus du LSTM : https://colah.github.io/posts/2015-08-Understanding-LSTMs/

#### Principes generaux
Chaque ligne porte un vecteur entier, de la sortie d'un nœud aux entrées des autres. Les cercles roses représentent des opérations ponctuelles, comme l'addition de vecteurs, tandis que les cases jaunes sont des couches de réseau neuronale apprises. Les lignes qui fusionnent dénotent la concaténation, tandis qu'une ligne qui bifurque indique que son contenu est copié et que les copies sont envoyées à différents endroits.
La clé des LSTMs est l'état de la cellule, la ligne horizontale qui traverse le haut du diagramme. L'état de la cellule est un peu comme la ligne vie du réseau. Il parcourt toute la chaîne, avec seulement quelques interactions mineures. Il est très facile pour l'information de circuler sans changement.
Le LSTM a la capacité de supprimer ou d'ajouter des informations à l'état de la cellule, soigneusement régulé par des structures appelées portes. Les portes sont un moyen de laisser passer l'information de façon sélective. Ils sont composés d'une couche de réseau neuronale sigmoïde et d'une opération de multiplication ponctuelle.
La couche sigmoïde affiche des nombres compris entre zéro et un, décrivant la quantité de chaque composant à laisser passer. Une valeur de zéro signifie "rien ne passe", tandis qu'une valeur de un signifie "tout passe" ! Un LSTM possède trois de ces portes, pour protéger et contrôler l'état de la cellule.

#### En detail
La première étape de notre LSTM est de décider quelles informations nous allons jeter en dehors de l'état cellulaire. Cette décision est prise par une couche sigmoïde appelée "couche oublie la porte".

![](https://zupimages.net/up/19/20/csif.png)

L'étape suivante consiste à décider quelles nouvelles informations vont être stockées dans l'état de la cellule. Il comporte deux parties. Tout d'abord, une couche sigmoïde appelée "couche de porte d'entrée" décide quelles valeurs nous allons mettre à jour. Ensuite, un calque tanh crée un vecteur de nouvelles valeurs candidates, C̃t qui pourrait être ajouté à l'État. Dans la prochaine étape, nous allons combiner ces deux pour créer une mise à jour de l'état.

![](https://zupimages.net/up/19/20/2q5x.png)

Il est maintenant temps de mettre à jour l'ancien état de la cellule, Ct-1 dans le nouvel état de cellule Ct. Les étapes précédentes avaient déjà décidé de ce qu'il fallait faire, il ne nous reste plus qu'à le faire.

Nous multiplions l'ancien état par ft oubliant les choses que nous avons décidé d'oublier tout à l'heure. Puis nous ajoutons les nouvelles valeurs candidates, mises à l'échelle en fonction du degré de mise à jour que nous avons décidé d'apporter à chaque valeur d'état.

![](https://zupimages.net/up/19/20/0agg.png)

Enfin, nous devons décider ce que nous allons produire. Cette sortie sera basée sur l'état de notre cellule, mais sera une version filtrée. Tout d'abord, nous exécutons une couche sigmoïde qui décide quelles parties de l'état de la cellule nous allons sortir. Ensuite, on fait passer l'état cellulaire par le tanh (pour pousser les valeurs à être comprises entre -1 et 1) et le multiplier par la sortie de la porte sigmoïde, de sorte que nous ne sortons que les parties que nous avons décidé de sortir.

![](https://zupimages.net/up/19/20/ibdb.png)

Le fonctionnement a l'air compliqué, mais tout ce que nous venons de détailler s'exécute dans une "boite noire". Il est possible d'influer sur certains paramètres, mais le fonctionnement des LSTM est toujours le même. Il faut retenir qu'un LSTM est un RNN qui garde en mémoire et sélectionne les informations importantes tout au long du processus.

## Mise en place d'un réseau neuronnal
Nous allons ici partir dans le détail, la technique, la théorie en créant étape par étape un réseau neuronale qui va prédire un 0 ou un 1 en fonction de ce que vous choisissez en entrée. Le code est développé en Python 3.

Theorie :
Considérons 3 entrées que nous nommerons x1, x2 et x3.

![input](https://zupimages.net/up/19/20/zrw6.png)

Ils sont associés à des poids respectifs nommées w1, w2, w3

![poids](https://zupimages.net/up/19/20/s5kj.png)

En additionnant la multiplication de chaque entrée avec son poids nous obtenons un résultat que nous noterons S

![fc](https://zupimages.net/up/19/20/oswf.png)

Le résultat de cette equation est passé à la fonction d'activation. Nous choisissons la fonction sigmoid définit par : f(x)= 1/1+e^-x
Elle est décrite par ce graphe :

![sigmoid](https://zupimages.net/up/19/20/4wln.png)

Nous donnons S a la fonction (Y = f(S)) qui va renvoyer une valeur entre 0 et 1
 - Si Y > 0,5 alors le réseau prédit 1
 - Si Y < 0,5 alors le réseau prédit 0

L'erreur entre la sortie réelle et la sortie prevue est calculé et mise a jour en utilisant une descente de gradient.

Dans un premier temps, ouvrez un script.
Vous aurez besoin d'importer des librairies :
```
import numpy
```
Creons un jeu de données supervisé. Il y a 3 valeurs pour la prevision d'une 4eme.
```
training_set_inputs = array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
training_set_outputs = array([[0, 1, 1, 0]]).T
```
Un array est une sorte de liste en python. A la fin de la seconde ligne, le `T` est pour transposer la liste sous la forme d'une colonne et non d'une ligne.

Definissions ensuite la fonction sigmoid :
```
def __sigmoid(self, x):
	return 1 / (1 + exp(-x))
```
Afin de faire la mise à jour des poids basés sur la descente de gradient pendant la phase d'apprentissage, nous implémentant la dérivé de la fonction sigmoid.
```
def __sigmoid_derivative(self, x):
	return x * (1 - x)
```
Pour commencer a apprendre, il faut commencer quelque part, nous créons donc des valeurs aléatoires pour les poids de chaque input.
```
self.synaptic_weights =  2  * random.random((3, 1)) -  1
```

Maintenant que nous avons tous le éléments, nous pouvons passer à la phase d'apprentissage
```
def train(self, training_set_inputs, training_set_outputs, number_of_training_iterations):
	for iteration in range(number_of_training_iterations):
		output = self.think(training_set_inputs)
		error = training_set_outputs - output
		adjustment = dot(training_set_inputs.T, error * self.__sigmoid_derivative(output))
		self.synaptic_weights += adjustment
```
Dans cette étape, nous calculons le résultat afin de calculer les erreurs et ajouter les poids de l'équation de base.
La fonction `think` sert a faire la comparaison entre la fonction sigmoid et le résultat.
```
def think(self, inputs):
	return self.__sigmoid(dot(inputs, self.synaptic_weights))
```
Nous rédigeons ensuite la fonction main :
```
if __name__ == "__main__":

    neural_network = NeuralNetwork()

    print ("Poids definis aléatoirement : ")
    print (neural_network.synaptic_weights)

    training_set_inputs = array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
    training_set_outputs = array([[0, 1, 1, 0]]).T

    neural_network.train(training_set_inputs, training_set_outputs, 10000)

    print ("Nouveaux poids : ")
    print (neural_network.synaptic_weights)

    condition=False
    mylist = []
    var = 0

    for co in range(3) :
        while condition == False :
            var = input("Rentrez le numero 0 ou 1 : ")
            print(co+1,"/3")
            if var == '0' or var == '1':
                var = int(var)
                mylist.append(var)
                print("Merci")
                condition = True
            else : print("Mauvaise entrée")
        condition = False

    rep = neural_network.think(array(mylist))

    if rep > 0.5 :
        print("Pour la liste" , mylist , "le reseaux predit un 1" )
    elif rep == 0.5 :
        print("Pour la liste" , mylist , "le reseaux ne sait pas predire :(" )
    else :
        print("Pour la liste" , mylist , "le reseaux predit un 0" )
```
Nous redisons un programme de démonstration. Pour un souci de clarté nous souhaitons afficher les poids générés aléatoirement puis les poids finals ainsi que la prévision du modèle. Si le résultat est supérieur à 0,5 le modèle a prédit un 1 sinon 0.








## Conclusion

Les réseaux de neurones sont populaires et sont à la pointe de l'informatique cognitive. C'est paramétrer un ordinateur à la manière du fonctionnement de notre cerveau pour qu'il fasse un plus grand nombre de calculs, plus rapidement, de façon autonome et avec un taux d'erreur le plus faible possible (notre cerveau aussi peut se tromper).
La technologie blockchain se base exclusivement sur la méthode des réseaux de neurones. Chaque block est activé en fonction d'information qu'il reçoit afin d'établies un processus sûr et cohérent.
Combiné a une puissance de calcul toujours plus grande et la multiplication des données, les réseaux de neurones sont de plus en plus présent dans l'univers du machine learning et de l'intelligence artificielle



## Sources
()[https://github.com/ashwanijha04/Make-a-neural-network/blob/master/NeuralNetwork.py]

()[https://medium.com/@UdacityINDIA/how-to-build-your-first-neural-network-with-python-6819c7f65dbf]

()[https://medium.com/datadriveninvestor/neural-networks-explained-6e21c70d7818]

()[https://en.wikipedia.org/wiki/Artificial_neural_network]

()[https://fr.coursera.org/learn/neural-networks-deep-learning]

()[https://skymind.ai/wiki/neural-network]

()[https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6]

http://www.easy-tensorflow.com/tf-tutorials/recurrent-neural-networks/vanilla-rnn-for-classification

https://www.oreilly.com/ideas/introduction-to-lstms-with-tensorflow

https://www.oracle.com/fr/cloud/deep-learning-intelligence-artificielle.html
